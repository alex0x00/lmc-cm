#ifndef METHODSCM_H
#define METHODSCM_H

#include <Eigen/Dense>
#define _USE_MATH_DEFINES
#include <cmath>

using Eigen::MatrixXf;
using Eigen::VectorXf;

/*
%CH23 Program for Chapter 23
% Backward time central space (BTCS) for heat eqn
%%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
L = pi; Nx = 9; dx = L/Nx;
T=3; Nt = 19; dt = T/Nt; nu = dt/dx^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
B = (1+2*nu)*eye(Nx-1,Nx-1) - nu*diag(ones(Nx-2,1),1) - nu*diag(ones(Nx-2,1),-1);
U = zeros(Nx-1,Nt+1);
U(:,1) = sin([dx:dx:L-dx]');
for i = 1:Nt
x = B\U(:,i);
U(:,i+1) = x;
end
bc = zeros(1,Nt+1);
U = [bc;U;bc];
*/
MatrixXf backwardTimeCentralSpace(int Nt, int Nx, float T)
{
  float L = M_PI, dx = L / Nx, dt = T / Nt;
  float nu = dt / (dx * dx);

  MatrixXf B = MatrixXf::Zero(Nx - 1, Nx - 1);
  B.diagonal(1).array() = -nu;
  B.diagonal().array() = 1 + 2 * nu;
  B.diagonal(-1).array() = -nu;

  MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
  U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

  for (int i{0}; i < Nt; ++i)
      U.col(i + 1) = B.ldlt().solve(U.col(i));

  MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
  bc.block(1, 0, Nx - 1, Nt + 1) = U;
  return bc;
}

/*
%CH23 Program for Chapter 23
% forward time central space (FTCS) for heat eqn
%%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
L = pi; Nx = 14; dx = L/Nx;
T=3; Nt = 199; dt = T/Nt; nu = dt/dx^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
F = (1-2*nu)*eye(Nx-1,Nx-1) + nu*diag(ones(Nx-2,1),1) + nu*diag(ones(Nx-2,1),-1);
U = zeros(Nx-1,Nt+1);
U(:,1) = sin([dx:dx:L-dx]');
for i = 1:Nt
x = F*U(:,i);
U(:,i+1) = x;
end
bc = zeros(1,Nt+1);
U = [bc;U;bc];
*/
MatrixXf forwardTimeCentralSpace(int Nt, int Nx, float T)
{
    float L = M_PI, dx = L / Nx, dt = T / Nt;
    float nu = dt / (dx * dx);

    MatrixXf F = MatrixXf::Zero(Nx - 1, Nx - 1);
    F.diagonal(1).array() = nu;
    F.diagonal().array() = 1 - 2 * nu;
    F.diagonal(-1).array() = nu;

    MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
    U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

    for (int i{0}; i < Nt; ++i)
        U.col(i + 1) = F * U.col(i);

    MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
    bc.block(1, 0, Nx - 1, Nt + 1) = U;
    return bc;
}

/*
%CH23 Program for Chapter 23
% Crank–Nicolson for heat eqn
%%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
L = pi; Nx = 14; dx = L/Nx;
T=3; Nt = 199; dt = T/Nt; nu = dt/dx^2;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
F = (1-2*nu)*eye(Nx-1,Nx-1) + nu*diag(ones(Nx-2,1),1) + nu*diag(ones(Nx-2,1),-1);
B = (1+2*nu)*eye(Nx-1,Nx-1) - nu*diag(ones(Nx-2,1),1) - nu*diag(ones(Nx-2,1),-1);
U = zeros(Nx-1,Nt+1);
U(:,1) = sin([dx:dx:L-dx]');
for i = 1:Nt
x = (F/B)*U(:,i);
U(:,i+1) = x;
end
bc = zeros(1,Nt+1);
U = [bc;U;bc];
*/
MatrixXf CrankNicolson(int Nt, int Nx, float T)
{
    float L = M_PI, dx = L / Nx, dt = T / Nt;
    float nu = dt / (dx * dx);

    MatrixXf B = MatrixXf::Zero(Nx - 1, Nx - 1);
    B.diagonal(1).array() = -nu;
    B.diagonal().array() = 1 + 2 * nu;
    B.diagonal(-1).array() = -nu;

    MatrixXf F = MatrixXf::Zero(Nx - 1, Nx - 1);
    F.diagonal(1).array() = nu;
    F.diagonal().array() = 1 - 2 * nu;
    F.diagonal(-1).array() = nu;

    MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
    U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

    MatrixXf X = F * B.inverse();
    for (int i{0}; i < Nt; ++i)
        U.col(i + 1) = X * U.col(i);

    MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
    bc.block(1, 0, Nx - 1, Nt + 1) = U;
    return bc;
}

#endif // METHODSCM_H
