.pragma library

function parseReal(strNumber) {
    return parseFloat((new String(strNumber)).replace(',', '.'));
}

function setRangeAxis(axisX, rangeAxisX){
    axisX.min = rangeAxisX.min;
    axisX.max = rangeAxisX.max;
}

function RangeValues(){
    var self = this;
    self.clear = function(){
        self.max = Number.MIN_VALUE;
        self.min = Number.MAX_VALUE;
    }

    self.clear();

    self.push = function(value){
        if (value > self.max)
            self.max = value;
        if (value < self.min)
            self.min = value;
    }
    self.isEmpty = function(){
        return self.max === Number.MIN_VALUE
        && self.min === Number.MAX_VALUE;
    }
}
