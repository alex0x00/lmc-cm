#include "backend.h"
#include "methods-cm.h"
#include <QApplication>
#include <QQmlApplicationEngine>
#include <QtQml/QQmlContext>

/*
 * Лабораторно-методический комплекс для выполнения лабораторных работ по курсу:
 * Численные методы оценивания финансовых опционов.
 * ВолГУ маг. дисер.
 */

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QApplication app(argc, argv);
    qmlRegisterType<Functions>("NumericalMethods", 1, 0, "Functions");
    qmlRegisterType<Matrix>("NumericalMethods", 1, 0, "Matrix");
    QQmlApplicationEngine engine;
    engine.addImportPath("qrc:///");

    engine.load(QUrl(QStringLiteral("qrc:/qml/main.qml")));
    return app.exec();
}
