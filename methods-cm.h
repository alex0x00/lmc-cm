#ifndef METHODSCM_H
#define METHODSCM_H

#include <Eigen/Dense>

using Eigen::MatrixXf;
using Eigen::VectorXf;

namespace cm {
template <typename T>
T sign(T arg)
{
    if (arg > 0.0)
        return 1.0;
    if (arg < 0.0)
        return -1.0;
    return 0.0;
}

template <typename T>
struct BlackScholesReturn {
    double C, Cdelta, P, Pdelta;
};
template <typename T>
BlackScholesReturn<T> blackScholesPDE(T S, T E, T r, T sigma, T tau)
{
    T C, Cdelta, P, Pdelta;
    if (tau > 0.0) {
        T d1 = (std::log(S / E) + (r + 0.5 * std::pow(sigma, 2.0)) * (tau))
            / (sigma * std::sqrt(tau));
        T d2 = d1 - sigma * std::sqrt(tau);

        T N1 = 0.5 * (1.0 + std::erf(d1 / std::sqrt(2.0)));
        T N2 = 0.5 * (1.0 + std::erf(d2 / std::sqrt(2.0)));

        C = S * N1 - E * std::exp(-r * (tau)) * N2;
        Cdelta = N1;
        P = C + E * std::exp(-r * tau) - S;
        Pdelta = Cdelta - 1.0;
    } else {
        C = std::max(S - E, 0.0);
        Cdelta = 0.5 * (sign(S - E) + 1.0);
        P = std::max(E - S, 0.0);
        Pdelta = Cdelta - 1.0;
    }
    return { C, Cdelta, P, Pdelta };
}

template <typename T>
T bullSpread(T x, T e1, T e2)
{
    return std::max(x - e1, 0.0) - std::max(x - e2, 0.0);
}
template <typename T>
T butterflySpread(T x, T e1, T e2, T e3)
{
    return std::max(x - e1, 0.0) - 2.0 * std::max(x - e2, 0.0)
        + std::max(x - e3, 0.0);
}
template <typename T>
T callOption(T x, T e)
{
    return std::max(x - e, 0.0);
}
template <typename T>
T putOption(T x, T e)
{
    return std::max(e - x, 0.0);
}
template <typename T>
T compoundInterest1(T x, T r, T dzero)
{
    return dzero * std::exp(r * x);
}
template <typename T>
T compoundInterest2(T x, T r, T dzero, T i)
{
    return dzero * std::pow((1.0 + r * x / i), i);
}

/*
  %CH23 Program for Chapter 23
  % Backward time central space (BTCS) for heat eqn
  %%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
  L = pi; Nx = 9; dx = L/Nx;
  T=3; Nt = 19; dt = T/Nt; nu = dt/dx^2;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  B = (1+2*nu)*eye(Nx-1,Nx-1) - nu*diag(ones(Nx-2,1),1) - nu*diag(ones(Nx-2,1),-1);
  U = zeros(Nx-1,Nt+1);
  U(:,1) = sin([dx:dx:L-dx]');
  for i = 1:Nt
  x = B\U(:,i);
  U(:,i+1) = x;
  end
  bc = zeros(1,Nt+1);
  U = [bc;U;bc];
  */
MatrixXf backwardTimeCentralSpace(int Nt, int Nx, float T);

/*
  %CH23 Program for Chapter 23
  % forward time central space (FTCS) for heat eqn
  %%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
  L = pi; Nx = 14; dx = L/Nx;
  T=3; Nt = 199; dt = T/Nt; nu = dt/dx^2;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  F = (1-2*nu)*eye(Nx-1,Nx-1) + nu*diag(ones(Nx-2,1),1) + nu*diag(ones(Nx-2,1),-1);
  U = zeros(Nx-1,Nt+1);
  U(:,1) = sin([dx:dx:L-dx]');
  for i = 1:Nt
  x = F*U(:,i);
  U(:,i+1) = x;
  end
  bc = zeros(1,Nt+1);
  U = [bc;U;bc];
  */
MatrixXf forwardTimeCentralSpace(int Nt, int Nx, float T);

/*
  %CH23 Program for Chapter 23
  % Crank–Nicolson for heat eqn
  %%%%%%%%%%%%% Parameters %%%%%%%%%%%%%%%
  L = pi; Nx = 14; dx = L/Nx;
  T=3; Nt = 199; dt = T/Nt; nu = dt/dx^2;
  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  F = (1-2*nu)*eye(Nx-1,Nx-1) + nu*diag(ones(Nx-2,1),1) + nu*diag(ones(Nx-2,1),-1);
  B = (1+2*nu)*eye(Nx-1,Nx-1) - nu*diag(ones(Nx-2,1),1) - nu*diag(ones(Nx-2,1),-1);
  U = zeros(Nx-1,Nt+1);
  U(:,1) = sin([dx:dx:L-dx]');
  for i = 1:Nt
  x = (F/B)*U(:,i);
  U(:,i+1) = x;
  end
  bc = zeros(1,Nt+1);
  U = [bc;U;bc];
  */
MatrixXf CrankNicolson(int Nt, int Nx, float T);
}

#endif // METHODSCM_H
