import QtQuick 2.7
import NumericalMethods 1.0
import "../../js/common.js" as Helper
import "../UI/page"

BlackScholesForm {
    Functions{id: cm;}

    onCalculate:{
        var S = Helper.parseReal(inputS);
        var E = Helper.parseReal(inputE);
        var r = Helper.parseReal(inputR);
        var sigma = Helper.parseReal(inputSigma);
        var tau = Helper.parseReal(inputTau);

        var ret = cm.blackScholes(S, E, r, sigma, tau);
        outputC = ret.C;
        outputCdelta = ret.Cdelta;
        outputP = ret.P;
        outputPdelta = ret.Pdelta;
    }
}
