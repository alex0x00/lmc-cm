import QtQuick 2.7
import NumericalMethods 1.0
import "../../js/common.js" as Helper
import "../UI/page"

CallOptionForm {
    Functions{id: cm;}

    onCalculate:{
        var e = Helper.parseReal(inputE);

        var k1 = 0;
        var k2 = 2 * e;
        var range = 100;
        var k = (k2 - k1) / range;
        var y_ = new Helper.RangeValues();
        seriesModel.clear();
        for (var i = 0; i < range; ++i){
            var x = k1 + k * i;
            var y = cm.callOption(x, e);
            y_.push(y)
            seriesModel.append(x, y);
        }

        Helper.setRangeAxis(
                    chartView.axisX(seriesModel),
                    {'min':k1, 'max':k2});
        Helper.setRangeAxis(chartView.axisY(seriesModel),  y_);
        chartView.zoom(0.9)
        chartView.update();
    }
}
