import QtQuick 2.7
import NumericalMethods 1.0
import "../../js/common.js" as Helper
import "../UI/page"

ForwardTimeCentralSpaceForm {
    Matrix{id: datasource;}

    onCalculate:{
        var Nt = Helper.parseReal(inputNt);
        var Nx = Helper.parseReal(inputNx);
        var T = Helper.parseReal(inputT);

        datasource.calcForwardTimeCentralSpace(Nt, Nx, T);
        datasource.prepare(isBox);

        chartView.chart.axisX.min = datasource.xMin
        chartView.chart.axisX.max = datasource.xMax;
        chartView.chart.axisZ.min = datasource.zMin
        chartView.chart.axisZ.max = datasource.zMax;
        chartView.chart.axisY.min = datasource.yMin
        chartView.chart.axisY.max = datasource.yMax

        datasource.update(chartView.series);
    }
}
