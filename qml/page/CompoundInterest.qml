import QtQuick 2.4
import NumericalMethods 1.0
import "../../js/common.js" as Helper
import "../UI/page"

CompoundInterestForm {
    Functions{id: cm;}

    onCalculate:{
        var r = Helper.parseReal(inputR);
        var t = Helper.parseReal(inputT);
        var dzero = Helper.parseReal(inputD0);
        var k1 = 0.0;
        var range = 100;
        var m = 12 * t;
        var k = t / range;
        var y_ = new Helper.RangeValues();
        seriesModel1.clear();
        for (var i = 0; i < range; ++i){
            var x = k * i;
            var y = cm.compoundInterest1(x, r, dzero)
            y_.push(y);
            seriesModel1.append(x, y);
        }

        Helper.setRangeAxis(
                    chartView.axisX(seriesModel1),
                    {'min':k1, 'max':t});
        Helper.setRangeAxis(chartView.axisY(seriesModel1),  y_);

        seriesModel2.clear();
        y_.clear();
        for (var i = 0; i < m; ++i){
            x = i / 12;
            y = cm.compoundInterest2(x, r, dzero, i);
            y_.push(y);
            seriesModel2.append(x, y);
        }
        Helper.setRangeAxis(
                    chartView.axisX(seriesModel2),
                    {'min':k1, 'max':t});
        Helper.setRangeAxis(chartView.axisY(seriesModel2),  y_);
        chartView.update();
    }
}
