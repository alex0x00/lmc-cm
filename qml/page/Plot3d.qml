import QtQuick 2.1
import QtQuick.Layouts 1.0
import QtQuick.Controls 1.0
import QtDataVisualization 1.1
import "."

ApplicationWindow {
    title: qsTr("LMC-CM")
    width: 800
    height: 600
    visible: true
    id: mainView

    Item {
        id: surfaceView
        anchors.fill: parent

        ColorGradient {
            id: g //surfaceGradient
            property real s: 1.0/6
            ColorGradientStop { position: g.s*5; color: "darkRed" }
            ColorGradientStop { position: g.s*4; color: "red" }
            ColorGradientStop { position: g.s*3; color: "yellow" }
            ColorGradientStop { position: g.s*2; color: "cyan" }
            ColorGradientStop { position: g.s*1; color: "blue" }
            ColorGradientStop { position: g.s*0; color: "darkBlue" }
        }

        Surface3D {
            id: surfacePlot
            width: surfaceView.width
            height: surfaceView.height
            theme: Theme3D {
                type: Theme3D.ThemeDigia
                font.pointSize: 35
                colorStyle: Theme3D.ColorStyleRangeGradient
                gridEnabled:true
                baseGradients: [g]
            }
//            shadowQuality: AbstractGraph3D.ShadowQualityMedium
//            selectionMode: AbstractGraph3D.SelectionSlice | AbstractGraph3D.SelectionItemAndRow
//            scene.activeCamera.cameraPreset: Camera3D.CameraPresetIsometricLeft

//            axisX.labelFormat: "%d"
//            axisY.labelFormat: "%4f"
//            axisZ.labelFormat: "%d"
//            axisX.min: 0
//            axisY.min: 0
//            axisZ.min: 0
//            axisX.max: 200
//            axisY.max: 1
//            axisZ.max: 200

            Surface3DSeries {
                id: surfaceSeries
                drawMode: Surface3DSeries.DrawSurface;
                flatShadingEnabled: false;
                meshSmooth: true
                itemLabelFormat: "@xLabel, @zLabel: @yLabel"
                itemLabelVisible: false

                onItemLabelChanged: {
                    if (surfaceSeries.selectedPoint === surfaceSeries.invalidSelectionPosition)
                        selectionText.text = "No selection"
                    else
                        selectionText.text = surfaceSeries.itemLabel
                }
            }
            function generateData() {
                console.log("generateData")
                dataSource.calcForwardTimeCentralSpace(199, 14, 3);
                dataSource.prepare(true);
                dataSource.update(surfaceSeries);
            }
            Component.onCompleted: generateData()
        }
    }


    function checkState() {
        if (surfaceSeries.drawMode & Surface3DSeries.DrawSurface)
            surfaceToggle.text = "Hide Surface"
        else
            surfaceToggle.text = "Show Surface"

        if (surfaceSeries.drawMode & Surface3DSeries.DrawWireframe)
            surfaceGridToggle.text = "Hide Surface Grid"
        else
            surfaceGridToggle.text = "Show Surface Grid"
    }
}
