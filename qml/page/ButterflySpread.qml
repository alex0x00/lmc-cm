import QtQuick 2.7
import NumericalMethods 1.0
import "../../js/common.js" as Helper
import "../UI/page"

ButterflySpreadForm {
    Functions{id: cm;}

    onCalculate:{
        var e1 = Helper.parseReal(inputE1);
        var e3 = Helper.parseReal(inputE3);
        var e2 = (e1 + e3) / 2;

        var k1 = e2 - e1;
        var k2 = e2 + e1;
        var range = 100;
        var k = (k2 - k1) / range;

        var y_ = new Helper.RangeValues();
        seriesModel.clear();
        for (var i = 0; i < range; ++i){
            var x = k1 + k * i;
            var y = cm.butterflySpread(x, e1, e2, e3);
            y_.push(y);
            seriesModel.append(x, y);
        }

        Helper.setRangeAxis(
                    chartView.axisX(seriesModel),
                    {'min':k1, 'max':k2});
        Helper.setRangeAxis(chartView.axisY(seriesModel),  y_);
        chartView.zoom(0.9)
        chartView.update();
    }
}
