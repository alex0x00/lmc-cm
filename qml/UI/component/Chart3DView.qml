import QtQuick 2.7
import QtDataVisualization 1.1
import QtQuick.Controls 2.0

Item /*ApplicationWindow*/{
    visible: true
    property alias series: surfaceSeries
    property alias chart: chart
//    anchors{
//        bottom: parent.top
//        bottomMargin: 8
//        right: parent.right
//        rightMargin: 8
//        left: parent.left
//        leftMargin: 8
//        top: parent.top
//        topMargin: 8
//    }
    ColorGradient {
        id: g //surfaceGradient
        ColorGradientStop { position: 1.0; color: "darkRed" }
        ColorGradientStop { position: 0.83; color: "red" }
        ColorGradientStop { position: 0.67; color: "yellow" }
        ColorGradientStop { position: 0.33; color: "cyan" }
        ColorGradientStop { position: 0.17; color: "blue" }
        ColorGradientStop { position: 0.0; color: "darkBlue" }
    }
    Chart3DViewForm {
        id:chart
        anchors.fill: parent
        shadowQuality: AbstractGraph3D.ShadowQualityMedium
        //selectionMode: AbstractGraph3D.SelectionSlice | AbstractGraph3D.SelectionItemAndRow

        theme: Theme3D {
            type: Theme3D.ThemeDigia
            font.pointSize: 35
            colorStyle: Theme3D.ColorStyleRangeGradient
            gridEnabled:true
            baseGradients: [g]
        }

        scene.activeCamera.cameraPreset: Camera3D.CameraPresetIsometricLeft

        axisX.labelFormat: "%d"
        axisY.labelFormat: "%4f"
        axisZ.labelFormat: "%d"
        axisX.min: 0
        axisY.min: 0
        axisZ.min: 0
        axisX.max: 200
        axisY.max: 1
        axisZ.max: 200

        Surface3DSeries {
            id: surfaceSeries
            drawMode:QSurface3DSeries.DrawWireframe |Surface3DSeries.DrawSurface;
            flatShadingEnabled: false;
            meshSmooth: false //true
//            itemLabelFormat: "@xLabel, @zLabel: @yLabel"
//            itemLabelVisible: false

//            onItemLabelChanged: {
////                if (surfaceSeries.selectedPoint === surfaceSeries.invalidSelectionPosition)
////                    selectionText.text = "No selection"
////                else
////                    selectionText.text = surfaceSeries.itemLabel
//            }
        }
    }
}
