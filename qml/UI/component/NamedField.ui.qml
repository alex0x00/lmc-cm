import QtQuick 2.7
import QtQuick.Controls 2.1

Item {
    id: item1
    width: 100
    height: 30
    Label {
        id: label
        text: "label"
        anchors.verticalCenter: parent.verticalCenter
        anchors.left: parent.left
        anchors.leftMargin: 0
    }
    TextField {
        id: textField
        anchors{
            top: parent.top
            topMargin: 0
            bottom: parent.bottom
            bottomMargin: 0
            right: parent.right
            rightMargin: 0
            left: label.right
            leftMargin: 0
        }
    }
}
