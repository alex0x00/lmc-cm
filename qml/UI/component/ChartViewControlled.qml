import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtCharts 2.0

ChartViewControlledForm {
    id: chartView
    anchors{
        bottom: parent.top
        bottomMargin: 8
        right: parent.right
        rightMargin: 8
        left: parent.left
        leftMargin: 8
        top: parent.top
        topMargin: 8
    }
    transitions: [
        Transition {
            from: "open"
            to: "close"
            SequentialAnimation{
                PropertyAnimation {
                    target: panelArea
                    property: "anchors.rightMargin"
                    duration: 400
                    easing.type: Easing.InOutQuad
                }
                ScriptAction {
                    script:panelArea.visible = false
                }
            }
        },
        Transition {
            from: "close"
            to: "open"
            SequentialAnimation{
                ScriptAction {
                    script:panelArea.visible = true
                }
                PropertyAnimation {
                    target: panelArea
                    property: "anchors.rightMargin"
                    duration: 400
                    easing.type: Easing.InOutQuad
                }
            }
        }
    ]


    onPlusClicked: {
        var state = chartView.animationOptions;
        chartView.animationOptions = ChartView.NoAnimation;
        chartView.zoomIn();
        chartView.animationOptions = state;
    }

    onMinusClicked: {
        var state = chartView.animationOptions;
        chartView.animationOptions = ChartView.NoAnimation;
        chartView.zoomOut();
        chartView.animationOptions = state;
    }

    MouseArea{
        id: hoverArea
        anchors.fill: parent
        propagateComposedEvents: true
        //preventStealing: false
        hoverEnabled: true
        onEntered: {chartView.state = "open";}
        onExited: {chartView.state = "close";}
    }

    //anchors.fill: parent
    //        legend.visible: true
    //        legend.alignment: Qt.AlignBottom
    animationOptions: ChartView.AllAnimations
    antialiasing: true
}
