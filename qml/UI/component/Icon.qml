import QtQuick 2.0
import "Icon.js" as IconsType
Item{
    id:root
    property string iconType: "?"
    property alias iconSize: fontIcon.font.pixelSize
    property alias iconColor: fontIcon.color
    Text {
        id:fontIcon
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        //renderType: Text.NativeRendering
        antialiasing: true
        smooth: true
        horizontalAlignment: Text.AlignHCenter
        font.family: fixedFont.name
        font.pixelSize: 50
        FontLoader {
            id: fixedFont;
            source: "materialdesignicons.ttf"
            onStatusChanged: {
                if (status == FontLoader.Ready)
                    console.log('Loaded font:', source)
            }
        }
        text: {
            var icon = IconsType.Icon[root.iconType];
            return (icon) ? icon: root.iconType;
        }
    }
}
