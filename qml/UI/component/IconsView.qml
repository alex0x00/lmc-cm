import QtQuick 2.6
import QtQuick.Layouts 1.1
import "Icon.js" as I
Flickable{
    contentWidth: grid.width; contentHeight: grid.height
    GridLayout{
        id:grid
        columns: 12
        Repeater{
            id:repeater
            Column{
                spacing: 4
                IconButton{
                    width: 50
                    height: 50
                    iconType: modelData
                }
                Text{
                    text: modelData
                }
            }
        }
    }
    Component.onCompleted: {
        var icons_name = []
        for (var key in I.Icon){
            icons_name.push(key);
        }
        repeater.model = icons_name;
    }
}


