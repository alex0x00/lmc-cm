import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtCharts 2.0

ChartView {
    id: chartView
    //anchors.fill: parent
    //        legend.visible: true
    //        legend.alignment: Qt.AlignBottom
    animationOptions: ChartView.AllAnimations
    antialiasing: true
    signal plusClicked();
    signal minusClicked();
    property alias panelArea: panelArea
    clip: true

    Item{
        id: panelArea
        anchors{
            right: parent.right
            verticalCenter: parent.verticalCenter
        }
        width: toolArea.width
        height: toolArea.height

        Column{
            id:toolArea
            spacing: 2
            property int buttonSize: 30
            IconButton{
                id:zoomPlus
                width: parent.buttonSize
                height: parent.buttonSize
                iconType:"plus"
                Connections{
                    onClicked: plusClicked()
                }
            }
            IconButton{
                id:zoomMinus
                width: parent.buttonSize
                height: parent.buttonSize
                iconType: "minus"
                Connections{
                    onClicked: minusClicked()
                }
            }
        }
    }

    states: [
        State{
            name: "open"
            PropertyChanges{target: panelArea; anchors.rightMargin: 8}
        },
        State{
            name: "close"
            PropertyChanges{target: panelArea; anchors.rightMargin: -panelArea.width}
        }
    ]
    state: "close"
}
