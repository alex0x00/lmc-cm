import QtQuick 2.0
Rectangle {
    id:rect
    radius: 15
    border.width: 4
    border.color: "#9952433f"
    property alias iconType: icon.iconType
    property alias iconColor: icon.iconColor
    signal clicked();
    Icon{
        id:icon
        anchors.fill: parent
        anchors.margins: 4
        iconSize: Math.min(width, height)
    }
    MouseArea {
        anchors.fill: parent
        //propagateComposedEvents: true
        Connections{
            onClicked: rect.clicked()
        }
    }
}
