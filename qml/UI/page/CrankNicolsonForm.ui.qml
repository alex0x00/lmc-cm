import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import "../component"

Item {
    id: item1
    property alias inputNt: textField1.text
    property alias inputNx: textField2.text
    property alias inputT: textField3.text
    property alias isBox: checkBox.checked
    property alias chartView: chartView
    signal calculate

    Chart3DView {
        id: chartView
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0
        anchors {
            bottom: grid.top
        }
    }
    IntValidator {
        id: inputInt
        bottom: 1
    }

    DoubleValidator {
        id: inputReal
    }
    GridLayout {
        id: grid
        anchors {
            left: parent.left
            leftMargin: 8
            bottom: parent.bottom
            bottomMargin: 8
        }
        columns: 2
        Label {
            text: qsTr("Nt:")
        }
        TextField {
            id: textField1
            validator: inputInt
        }
        Label {
            text: qsTr("Nx:")
        }
        TextField {
            id: textField2
            validator: inputInt
        }
        Label {
            text: qsTr("T:")
        }
        TextField {
            id: textField3
            validator: inputReal
        }
        Button {
            Layout.columnSpan: 2
            Layout.fillWidth: true
            id: buttonCalc
            text: qsTr("Calculate")
            Connections {
                onClicked: {
                    calculate()
                }
            }
        }
    }

    CheckBox {
        id: checkBox
        text: qsTr("Box")
        anchors.top: chartView.bottom
        anchors.topMargin: 8
        anchors.left: grid.right
        anchors.leftMargin: 8
    }
}
