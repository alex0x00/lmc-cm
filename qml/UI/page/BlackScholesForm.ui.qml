import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import "../component"

Item {
    id: item1
    property alias inputS: textField1.text
    property alias inputE: textField2.text
    property alias inputR: textField3.text
    property alias inputSigma: textField4.text
    property alias inputTau: textField5.text
    property alias outputC: textField6.text
    property alias outputCdelta: textField7.text
    property alias outputP: textField8.text
    property alias outputPdelta: textField9.text

    signal calculate
    //C, Cdelta, P, Pdelta <- blackScholesPDE(S, E, r, sigma, tau);

    DoubleValidator {
        id: inputReal
    }
    GridLayout {
        id:grid;
        columns: 2
        anchors.centerIn: parent

//        Label {
//            id: title
//            Layout.columnSpan: 2
//            Layout.fillWidth: true
//            text: qsTr("Black Scholes PDE")
//            font.pointSize: 16
//            horizontalAlignment: Text.AlignHCenter
//        }
        GridLayout {
            id: gridInput
            columns: 2
            Label {
                text: qsTr("S:")
            }
            TextField {
                id: textField1
                text: "0"
                validator: inputReal
            }
            Label {
                text: qsTr("E:")
            }
            TextField {
                id: textField2
                text: "0"
                validator: inputReal
            }
            Label {
                text: qsTr("r:")
            }
            TextField {
                id: textField3
                text: "0"
                validator: inputReal
            }
            Label {
                text: qsTr("sigma:")
            }
            TextField {
                id: textField4
                text: "0"
                validator: inputReal
            }
            Label {
                text: qsTr("Tau:")
            }
            TextField {
                id: textField5
                text: "0"
                validator: inputReal
            }

        }

        GridLayout {
            id: gridOutput
            columns: 2
            Label {
                text: qsTr("C:")
            }
            TextField {
                id: textField6
                text: "0"
                readOnly: true
            }
            Label {
                text: qsTr("Cdelta:")
            }
            TextField {
                id: textField7
                text: "0"
                readOnly: true
            }
            Label {
                text: qsTr("P:")
            }
            TextField {
                id: textField8
                text: "0"
                readOnly: true
            }
            Label {
                text: qsTr("Pdelta:")
            }
            TextField {
                id: textField9
                text: "0"
                readOnly: true
            }
        }


        Button {
            id: buttonCalc
            Layout.columnSpan: 2
            Layout.fillWidth: true
            text: qsTr("Calculate")
            Connections {
                onClicked: {
                    calculate()
                }
            }
        }
    }
}
