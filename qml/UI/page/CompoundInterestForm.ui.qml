import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtCharts 2.0
import "../component"

Item {
    id: item1
    property alias inputR: textField1.text
    property alias inputT: textField2.text
    property alias inputD0: textField3.text
    property alias seriesModel1: seriesModel1
    property alias seriesModel2: seriesModel2
    property alias chartView: chartView
    signal calculate
    ChartViewControlled {
        id: chartView
        anchors{
            bottom: grid.top
        }
        LineSeries {
            id: seriesModel1
            name: qsTr("e^rx")
        }
        LineSeries {
            id: seriesModel2
            name: qsTr("Compound interest")
        }
    }

    DoubleValidator {
        id: inputReal
    }
    GridLayout {
        id: grid
        anchors.left: parent.left
        anchors.leftMargin: 8
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        columns: 2
        Label {
            text: qsTr("r:")
        }
        TextField {
            id: textField1
            text: "0"
            validator: inputReal
        }
        Label {
            text: qsTr("T:")
        }
        TextField {
            id: textField2
            text: "0"
            validator: inputReal
        }
        Label {
            text: qsTr("d(0):")
        }
        TextField {
            id: textField3
            text: "0"
            validator: inputReal
        }
        Button {
            Layout.columnSpan: 2
            Layout.fillWidth: true
            id: buttonCalc
            text: qsTr("Calculate")
            Connections {
                onClicked: {
                    calculate()
                }
            }
        }
    }
}
