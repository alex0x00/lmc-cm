import QtQuick 2.7
import QtQuick.Controls 2.1
import QtQuick.Layouts 1.0
import QtCharts 2.0
import "../component"

Item {
    id: item1
    property alias inputE: textField.text
    property alias seriesModel: seriesModel
    property alias chartView: chartView
    signal calculate
    ChartViewControlled {
        id: chartView
        anchors{
            bottom: grid.top
        }
        LineSeries {
            id: seriesModel
            name: qsTr("Put Option")
        }
    }

    DoubleValidator {
        id: inputReal
    }
    GridLayout {
        id: grid
        anchors{
            left: parent.left
            leftMargin: 8
            bottom: parent.bottom
            bottomMargin: 8
        }
        columns: 2
        Label {
            text: qsTr("E:")
        }
        TextField {
            id: textField
            text: "0"
            validator: inputReal
        }
        Button {
            Layout.columnSpan: 2
            Layout.fillWidth: true
            id: buttonCalc
            text: qsTr("Calculate")
            Connections {
                onClicked: {
                    calculate()
                }
            }
        }
    }
}
