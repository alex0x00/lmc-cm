import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0
import "page"
import "UI/component/"

ApplicationWindow {
    title: qsTr("LMC-CM")
    minimumWidth: 800
    minimumHeight: 600
    visible: true

    ListModel {
        id: modelMethods
        ListElement{
            unit: "1"
            name: "Call Option"
            form: "page/CallOption.qml"
        }
        ListElement{
            unit: "1"
            name: "Put Option"
            form: "page/PutOption.qml"
        }
        ListElement{
            unit: "1"
            name: "Bull Spread"
            form: "page/BullSpread.qml"
        }
        ListElement{
            unit: "1"
            name: "Butterfly Spread"
            form: "page/ButterflySpread.qml"
        }
        ListElement{
            unit: "1"
            name: "Compound Interest"
            form: "page/CompoundInterest.qml"
        }
        ListElement{
            unit: "2"
            name: "Black Scholes PDE"
            form: "page/BlackScholes.qml"
        }
        ListElement{
            unit: "3"
            name: "Backward Time Central Space"
            form:  "page/BackwardTimeCentralSpace.qml"
        }
        ListElement{
            unit: "3"
            name: "Forward Time Central Space"
            form: "page/ForwardTimeCentralSpace.qml"
        }
        ListElement{
            unit: "3"
            name: "Crank Nicolson"
            form: "page/CrankNicolson.qml"
        }
    }
    header: ToolBar {
        RowLayout {
            spacing: 20
            anchors.fill: parent
            ToolButton {
                contentItem:Icon{
                    iconType: "menu"
                    iconSize: 40
                }
                onClicked: drawer.open()
            }
            Label {
                id: titleLabel
                text: "Main"
                font.pixelSize: 20
                elide: Label.ElideRight
                horizontalAlignment: Qt.AlignHCenter
                verticalAlignment: Qt.AlignVCenter
                Layout.fillWidth: true
            }
        }
    }

    Drawer {
        id: drawer
        width: 300
        height: parent.height
        //        Label {
        //            id: titleList
        //            text: qsTr("List of laboratory")
        //            anchors.right: parent.right
        //            anchors.rightMargin: 0
        //            anchors.left: parent.left
        //            anchors.leftMargin: 0

        //            font.pointSize: 16
        //            background: Rectangle {
        //                implicitWidth: 100
        //                implicitHeight: 50
        //                color: "#eeeeee"
        //            }
        //        }

        ListView {
            id: listView
            model:modelMethods
            //width: parent.width
            anchors.fill: parent
            //                anchors.top: titleList.bottom
            //                anchors.topMargin: 2
            //                anchors.right: parent.right
            //                anchors.rightMargin: 0
            //                anchors.left: parent.left
            //                anchors.leftMargin: 0
            focus: true
            section.property: "unit"
            section.criteria: ViewSection.FullString
            section.delegate: Label {
                width: listView.width
                text: qsTr("Unit № %1").arg(section);
                font.pointSize: 14
                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: "#eeeeee"
                }
            }
            delegate: ItemDelegate {
                id: control
                width: parent.width
                text: name
                font.pointSize: 12
                highlighted: ListView.isCurrentItem
                onClicked:{
                    titleLabel.text = name
                    pageView.source = form;
                }

                background: Rectangle {
                    implicitWidth: 100
                    implicitHeight: 40
                    opacity: enabled ? 1 : 0.3
                    color: control.down ? "#dddedf" : "#eeeeee"
                    Rectangle {
                        width: parent.width
                        height: 1
                        color: control.down ? "#17a81a" : "#21be2b"
                        anchors.bottom: parent.bottom
                    }
                }
            }
            ScrollIndicator.vertical: ScrollIndicator {}
        }

    }

    Loader{
        id: pageView
        anchors.fill: parent
    }
}
