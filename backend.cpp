#include "backend.h"
#include "methods-cm.h"

using namespace QtDataVisualization;

Matrix::Matrix(QObject* parent)
    : QObject(parent)
    , m_datasource(nullptr)
{
    qRegisterMetaType<QSurface3DSeries*>();
}

Matrix::~Matrix()
{
    clearData();
}

int Matrix::xMin() const
{
    return m_xMin;
}

int Matrix::zMin() const
{
    return m_zMin;
}

int Matrix::xMax() const
{
    return m_xMax;
}

int Matrix::zMax() const
{
    return m_zMax;
}

float Matrix::yMin() const
{
    return m_heigthMin;
}

float Matrix::yMax() const
{
    return m_heigthMax;
}

void Matrix::prepare(bool is_box)
{
    auto cols = m_matrix.cols();
    auto rows = m_matrix.rows();

    if (!m_datasource){
        m_datasource = new QSurfaceDataArray;
        m_datasource->reserve(cols);
        auto j{m_datasource->begin()}, end{m_datasource->begin() + cols};
        for (; j != end; ++j){
            m_datasource->append(new QSurfaceDataRow(rows));
        }
    } else {
        m_datasource->reserve(cols);

        int seved_size = std::min(m_datasource->size(), cols);
        auto j{m_datasource->begin()}, end{m_datasource->begin() + seved_size};
        QSurfaceDataRow* row{*j};
        if (row->size() > rows){
            for (; j != end; ++j){
                row = *j;
                row->erase(row->begin() + rows, row->end());
            }
        } else if (rows > row->size()) {
            for (; j != end; ++j){
                row = *j;
                row->insert(row->end(), rows - row->size(), QSurfaceDataItem());
            }
        }

        if (m_datasource->size() > cols){
            auto  j{m_datasource->begin() + cols};
            for (; j != m_datasource->end(); ++j){
                delete *j;
            }
            m_datasource->erase(m_datasource->begin() + cols, m_datasource->end());
        } else if (cols > m_datasource->size()){
            for (int j{m_datasource->size()}; j < cols; ++j){
                m_datasource->append(new QSurfaceDataRow(rows));
            }
        }
    }
    m_xMin = m_zMin = 0;
    m_xMax = rows - 1;
    m_zMax = cols - 1;

    float k_x = 1., k_z = 1.;
    if (is_box) {
        if ((m_xMax - m_xMin) > (m_zMax - m_zMin)) {
            k_z = (m_xMax - m_xMin) / (m_zMax - m_zMin);
        } else {
            k_x = (m_zMax - m_zMin) / (m_xMax - m_xMin);
        }
        m_zMax = m_xMax = std::max(m_zMax, m_xMax);
    }

    m_heigthMin = m_heigthMax = m_matrix(0, 0);
    auto jj{m_datasource->cbegin()};
    for (int j{}; j < cols; ++j, ++jj) {
        QSurfaceDataRow* row{*jj};
        for (int i{}; i < rows; i++) {
            auto y = m_matrix(i, j);
            m_heigthMax = std::max(m_heigthMax, y);
            m_heigthMin = std::min(m_heigthMin, y);
            (*row)[i].setPosition(QVector3D(i * k_x, y, j * k_z));
        }
    }
}

void Matrix::calcBackwardTimeCentralSpace(int Nt, int Nx, float T)
{
    m_matrix = cm::backwardTimeCentralSpace(Nt, Nx, T);
}

void Matrix::calcForwardTimeCentralSpace(int Nt, int Nx, float T)
{
    m_matrix = cm::forwardTimeCentralSpace(Nt, Nx, T);
}

void Matrix::calcCrankNicolson(int Nt, int Nx, float T)
{
    m_matrix = cm::CrankNicolson(Nt, Nx, T);
}

void Matrix::update(QSurface3DSeries* series)
{
    if (series && m_datasource) {
        series->dataProxy()->resetArray(m_datasource);
    }
}

void Matrix::clearData()
{
    if (!m_datasource)
        return;
    auto  j{m_datasource->begin()};
    for (; j != m_datasource->end(); ++j){
        delete *j;
    }
    m_datasource->clear();
    delete m_datasource;
}

QVariant Functions::blackScholes(double S, double E, double r, double sigma, double tau)
{
    auto result = cm::blackScholesPDE(S, E, r, sigma, tau);
    return QVariantMap(
    {
        { "C", result.C },
        { "Cdelta", result.Cdelta },
        { "P", result.P },
        { "Pdelta", result.Pdelta }
    });
}

QVariant Functions::bullSpread(double x, double e1, double e2)
{
    return cm::bullSpread(x, e1, e2);
}

QVariant Functions::butterflySpread(double x, double e1, double e2, double e3)
{
    return cm::butterflySpread(x, e1, e2, e3);
}

QVariant Functions::callOption(double x, double e)
{
    return cm::callOption(x, e);
}

QVariant Functions::putOption(double x, double e)
{
    return cm::putOption(x, e);
}

QVariant Functions::compoundInterest1(double x, double r, double dzero)
{
    return cm::compoundInterest1(x, r, dzero);
}

QVariant Functions::compoundInterest2(double x, double r, double dzero, double i)
{
    return cm::compoundInterest2(x, r, dzero, i);
}
