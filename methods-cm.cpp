#include "methods-cm.h"
#define _USE_MATH_DEFINES
#include <cmath>
namespace cm {

Eigen::MatrixXf backwardTimeCentralSpace(int Nt, int Nx, float T)
{
    float L = M_PI, dx = L / Nx, dt = T / Nt;
    float nu = dt / (dx * dx);

    MatrixXf B = MatrixXf::Zero(Nx - 1, Nx - 1);
    B.diagonal(1).array() = -nu;
    B.diagonal().array() = 1 + 2 * nu;
    B.diagonal(-1).array() = -nu;

    MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
    U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

    for (int i{}; i < Nt; ++i)
        U.col(i + 1) = B.ldlt().solve(U.col(i));

    MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
    bc.block(1, 0, Nx - 1, Nt + 1) = U;
    return bc;
}

Eigen::MatrixXf forwardTimeCentralSpace(int Nt, int Nx, float T)
{
    float L = M_PI, dx = L / Nx, dt = T / Nt;
    float nu = dt / (dx * dx);

    MatrixXf F = MatrixXf::Zero(Nx - 1, Nx - 1);
    F.diagonal(1).array() = nu;
    F.diagonal().array() = 1 - 2 * nu;
    F.diagonal(-1).array() = nu;

    MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
    U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

    for (int i{}; i < Nt; ++i)
        U.col(i + 1) = F * U.col(i);

    MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
    bc.block(1, 0, Nx - 1, Nt + 1) = U;
    return bc;
}

Eigen::MatrixXf CrankNicolson(int Nt, int Nx, float T)
{
    float L = M_PI, dx = L / Nx, dt = T / Nt;
    float nu = dt / (dx * dx);

    MatrixXf B = MatrixXf::Zero(Nx - 1, Nx - 1);
    B.diagonal(1).array() = -nu;
    B.diagonal().array() = 1 + 2 * nu;
    B.diagonal(-1).array() = -nu;

    MatrixXf F = MatrixXf::Zero(Nx - 1, Nx - 1);
    F.diagonal(1).array() = nu;
    F.diagonal().array() = 1 - 2 * nu;
    F.diagonal(-1).array() = nu;

    MatrixXf U = MatrixXf::Zero(Nx - 1, Nt + 1);
    U.col(0).array() = VectorXf::LinSpaced(Nx - 1, dx, L - dx).array().sin();

    MatrixXf X = F * B.inverse();
    for (int i{}; i < Nt; ++i)
        U.col(i + 1) = X * U.col(i);

    MatrixXf bc = MatrixXf::Zero(Nx + 1, Nt + 1);
    bc.block(1, 0, Nx - 1, Nt + 1) = U;
    return bc;
}
}
