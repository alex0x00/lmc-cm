#ifndef DATASOURCE_H
#define DATASOURCE_H
#include <Eigen/Dense>
#include <QVariant>
#include <QtDataVisualization/QSurface3DSeries>

using namespace QtDataVisualization;
Q_DECLARE_METATYPE(QSurface3DSeries*)
class Functions : public QObject {
    Q_OBJECT
public:
    Q_INVOKABLE QVariant blackScholes(double S, double E, double r, double sigma, double tau);

    Q_INVOKABLE QVariant bullSpread(double x, double e1, double e2);

    Q_INVOKABLE QVariant butterflySpread(double x, double e1, double e2, double e3);

    Q_INVOKABLE QVariant callOption(double x, double e);

    Q_INVOKABLE QVariant putOption(double x, double e);

    Q_INVOKABLE QVariant compoundInterest1(double x, double r, double dzero);

    Q_INVOKABLE QVariant compoundInterest2(double x, double r, double dzero, double i);
};

class Matrix : public QObject {
    Q_OBJECT
public:
    explicit Matrix(QObject* parent = 0);
    virtual ~Matrix();
    Q_PROPERTY(int xMin READ xMin)
    Q_PROPERTY(int xMax READ xMax)
    Q_PROPERTY(int zMax READ zMax)
    Q_PROPERTY(int zMin READ zMin)
    Q_PROPERTY(float yMin READ yMin)
    Q_PROPERTY(float yMax READ yMax)

    int xMin() const;
    int xMax() const;
    int zMin() const;
    int zMax() const;
    float yMin() const;
    float yMax() const;

public Q_SLOTS:

    void calcBackwardTimeCentralSpace(int Nt, int Nx, float T);
    void calcForwardTimeCentralSpace(int Nt, int Nx, float T);
    void calcCrankNicolson(int Nt, int Nx, float T);
    void prepare(bool is_box);
    void update(QSurface3DSeries* series);

private:
    void clearData();

    QSurfaceDataArray* m_datasource;
    Eigen::MatrixXf m_matrix;
    float m_xMin, m_xMax, m_zMin, m_zMax;
    float m_heigthMin, m_heigthMax;
};

#endif
